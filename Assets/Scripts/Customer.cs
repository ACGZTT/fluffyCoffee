using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Coffee
{
    public class Customer : MonoBehaviour
    {
        public int Index;
        private CustomerData _data;
        public Bubble bubble;
        public SpriteRenderer sprite;
        public SpriteRenderer bubbleSprite;

        public CustomerData Data
        {
            get
            {
                return _data;
            }

            set
            {
                this._data = value;
                if (this._data == null)
                {
                    HideBubble();
                    gameObject.SetActive(false);
                }
                else
                {
                    gameObject.SetActive(transform);
                    sprite.color = GetColor((OrderType)_data.cType);
                    if (this._data.oType == OrderType.NULL)
                    {
                        HideBubble();
                    }
                    else
                    {
                        ShowBubble();
                    }
                    
                }
            }
        }

        public void ShowBubble()
        {
            bubble.gameObject.SetActive(true);

            if (bubbleSprite == null)
            {
                Debug.Log("û��ȡ��Sprite");
                return;
            }
            bubbleSprite.color = GetColor(_data.oType);
        }

        public void HideBubble()
        {
            bubble.gameObject.SetActive(false);
        }

        private Color GetColor(OrderType type)
        {
            switch (type)
            {
                case OrderType.red:
                    return Color.red;
                case OrderType.green:
                    return Color.green;
                case OrderType.blue:
                    return Color.blue;
                case OrderType.yellow:
                    return Color.yellow;
            }
            return Color.white;
        }
    }
}

