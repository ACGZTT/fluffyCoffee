using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Coffee
{
	public enum CustomerType
    {
		NULL,
		red,
		green,
		blue,
		yellow,
		max
	}
	public enum OrderType
    {
		NULL,
		red,
		green,
		blue,
		yellow,
		max
    }
    public class Controller : MonoBehaviour
    {
        public Transform Customers;
		public Transform Foods;
		public GameObject Bubble;
        public GameObject Door;
		public GameObject WorkSpace1;
		public GameObject WorkSpace2;
		public GameObject WorkSpace3;
		public GameObject WorkSpace4;
        public Text Coin_Text;

		private GameData _gameData;
		private List<Customer> customers;
		private List<GameObject> foods;

		private const int CUSTOMER_INTERVAL = 3;
		private const float ORDER_TIME = 1.5f;
		private const float COOK_TIME = 1.5f;
        private Color[] CUSTOMER_COLOR = new Color[4] { Color.red, Color.green, Color.blue, Color.yellow };
        private Color[] FOOD_COLOR = new Color[4] { Color.red, Color.green, Color.blue, Color.yellow };

		private void Start()
        {
			_gameData = new GameData();
            Door.AddComponent<Door>().controller = this;
			WorkSpace1.AddComponent<Workspace>().controller = this;
			WorkSpace2.AddComponent<Workspace>().controller = this;
			WorkSpace3.AddComponent<Workspace>().controller = this;
			WorkSpace4.AddComponent<Workspace>().controller = this;
			customers = new List<Customer>();
            for (int i = 0; i < Customers.childCount; i++)
            {
				var ct = Customers.GetChild(i);

				var cComponent = ct.gameObject.AddComponent<Customer>();
				cComponent.Index = i;
				cComponent.sprite = ct.gameObject.GetComponent<SpriteRenderer>();
				var bubble = ct.GetChild(0).gameObject;
				cComponent.bubble = bubble.AddComponent<Bubble>(); 
				cComponent.bubble.controller = this;
				cComponent.bubbleSprite = bubble.GetComponent<SpriteRenderer>();
				customers.Add(cComponent);
			}
			foods = new List<GameObject>();
			for (int i = 0; i < Foods.childCount; i++)
			{
				foods.Add(Foods.GetChild(i).gameObject);
			}
		}

		public void ClickDoor()
		{
			if(_gameData.Customers.Count > 3)
            {
				return;
            }
			var type = (CustomerType)Random.Range((int)CustomerType.red, (int)CustomerType.max);
			_gameData.Customers.Add(new CustomerData(type));
			UpdateCustomers();
			StartCoroutine(MakeOrder(_gameData.Customers.Count - 1));
		}

		public IEnumerator MakeOrder(int index)
        {
			yield return new WaitForSeconds(ORDER_TIME);
			var customer = customers[index];
			var type = (OrderType)Random.Range((int)OrderType.red, (int)OrderType.max);
			customer.Data.oType = type;
			customer.ShowBubble();
		}

		public void ClickWorkSpace(OrderType type)
		{
			StartCoroutine( MakeDeal(type));
		}

		public IEnumerator MakeDeal(OrderType type)
		{
			yield return new WaitForSeconds( COOK_TIME );
			_gameData.Foods.Add((OrderType)type);
			UpdateFoods();
		}

		public void ClickBubble(GameObject obj)
		{

			var customer = obj.transform.parent.gameObject.GetComponent<Customer>();
			var foods = _gameData.Foods;
            for (int i = 0; i < foods.Count; i++)
            {
				if(foods[i] == customer.Data.oType)
                {
					_gameData.Foods.Remove(customer.Data.oType);
					_gameData.Customers.RemoveAt(customer.Index);
					_gameData.Coin += 10;
					UpdateUI();
					UpdateCustomers();
					UpdateFoods();
					break;
                }
            }
		}

		public void UpdateUI()
        {
			var sb = new StringBuilder();
			sb.Append("我的钱:");
			sb.Append(_gameData.Coin);
			Coin_Text.text = sb.ToString();
        }

		public void UpdateCustomers()
		{
			var data = _gameData.Customers;
			int i = 0;
			for (; i < customers.Count && i < data.Count; i++)
			{
				customers[i].Data = data[i];
			}
			for (; i < customers.Count; i++)
			{
				customers[i].Data = null;
			}
		}

		public void UpdateFoods()
		{
			var data = _gameData.Foods;
			int i = 0;
			for (; i < foods.Count && i < data.Count; i++)
			{
				foods[i].SetActive(true);
				var type = (int)data[i];
				var color = FOOD_COLOR[type - 1];
				foods[i].GetComponent<SpriteRenderer>().color = color;
			}
			for (; i < foods.Count; i++)
			{
				foods[i].SetActive(false);
			}
		}
	}

	public class CustomerData
    {
		public CustomerType cType;
		public OrderType oType;
		public CustomerData(CustomerType type)
        {
			cType = type;
			oType = OrderType.NULL;
        }
    }
}

