using UnityEngine.EventSystems;

namespace Coffee
{
    public class Door : EventTrigger
    {
		public Controller controller;
        public override void OnPointerClick(PointerEventData eventData)
        {
            base.OnPointerClick(eventData);
			controller.ClickDoor();
        }
    }
    
    public class Workspace : EventTrigger
    {
		public Controller controller;
		public override void OnPointerClick(PointerEventData eventData)
        {
            base.OnPointerClick(eventData);
            var name = eventData.pointerClick.name;
            var numString = name.Trim("Workspace".ToCharArray());
            controller.ClickWorkSpace((OrderType)int.Parse(numString));
        }
    }
    
    public class Bubble : EventTrigger
    {
        public Controller controller;
        public override void OnPointerClick(PointerEventData eventData)
        {
            base.OnPointerClick(eventData);
            controller.ClickBubble(eventData.pointerClick);
        }
    }
}