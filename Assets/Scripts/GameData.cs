using System.Collections.Generic;

namespace Coffee
{
    public class GameData
    {
        public List<CustomerData> Customers = new List<CustomerData>();
        public List<OrderType> Foods = new List<OrderType>();
        public float Coin;
    }
}